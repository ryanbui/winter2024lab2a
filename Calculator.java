import java.util.Random;

public class Calculator {
    public static int add(int a, int b){
        return a + b;
    }

    public static double sqrt(int num){
        return Math.sqrt(num);
    }
    
    public static double randomNum(){
        Random rand = new Random();
        int rand_int= rand.nextInt(1000);
        return rand_int;
    }

    public static double divide(double num1, double num2){
        if(num2 == 0){
            System.out.println("Undefined");
            return 0;
        }
        return num1/num2;
    }
}
