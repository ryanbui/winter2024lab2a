import java.util.Scanner;

public class Driver {
    public static void main (String [] args){
		java.util.Scanner reader = new java.util.Scanner(System.in);

    System.out.println("Hello. Please input first integer");
    int num1 = reader.nextInt();
    System.out.println("Please input second integer");
    int num2 = reader.nextInt();
		System.out.println("The sum of " + num1 + " and " + num2 + " is " + Calculator.add(num1, num2));
		
		System.out.println("Please input an integer");
		int num3 = reader.nextInt();
		System.out.println("The square root of " + num3 + " is "+ Calculator.sqrt(num3));
		
		System.out.println("Random number: " + Calculator.randomNum());

    }
}
